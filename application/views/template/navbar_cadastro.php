<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
  <div class="container">
    <a class="navbar-brand" href="<?= base_url(''); ?>">JR Barbearia - Administração.</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="oi oi-menu"></span> Menu
    </button>
    <div class="collapse navbar-collapse" id="ftco-nav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item"><a href="<?= base_url('index.php/logar/admin'); ?>" class="nav-link">Cadastrar clientes</a></li>
        <li class="nav-item"><a href="<?= base_url('index.php/logar/lista'); ?>" class="nav-link">Meus clientes</a></li>
        <!--
            <li class="nav-item"><a href="<?= base_url('index.php/barbearia/sobre'); ?>" class="nav-link">Calendario</a></li>
            <li class="nav-item"><a href="<?= base_url('index.php/barbearia/galeria'); ?>" class="nav-link">Editar página</a></li>
        -->
        <li class="nav-item btn-danger"><a href="<?= base_url("index.php/logar/logout"); ?>" class="nav-link">Sair</a></li>
      </ul>
    </div>
  </div>
</nav>