<div class="container mt-2">
    <div class="row pt-1 mt-2">
        <div class="col-lg-6 mx-auto border border-warning">
        <h1 class="mr-2 text-center mt-5"> <a href="#"> Cadastrar Usuário </a></h1>
            <form  method="POST" class="w-100" action="<?= base_url() ?>index.php/logar/cadastrar">
                <label class="pt-5"> Nome: </label><br>
                <input value="<?= isset($user) ? $user['nome'] : '' ?>" type="text" class="w-100" id="nome" name="nome" size="35" maxlength="60"  placeholder="Digite seu nome completo" required><br><br>
                <label>E-mail: </label><br>
                <input value="<?= isset($user) ? $user['email'] : '' ?>" type="email" class="w-100" id="email" name="email" size="35" maxlength="60" placeholder="Digite um e-mail válido" required><br><br> 
                <label>Senha: </label><br>
                <input value="<?= isset($user) ? $user['senha'] : '' ?>" type="password" class="w-100" id="senha" name="senha" size="35" maxlength="60" placeholder="Digite uma senha" required><br><br> 
                <label>Telefone: </label><br>
                <input value="<?= isset($user) ? $user['telefone'] : '' ?>" type="text" class="w-100" id="telefone" name="telefone" size="35" maxlength="13" placeholder="Digite um numero de celular" required><br><br>
                <label>Nível: </label><br>
                <select type="text" class="form-select" id="nivel" name="nivel"  required>
                    <option value="0"> --- </option>
                    <option value="1"> Administrador </option>
                    <option value="2"> Cliente </option>
                </select><br><br> 
                <div class="mx-auto text-center">
                    <input type="submit" class="btn btn-blue px-4 py-3 text-center" value="Salvar">
                </div>
            </form>
        </div>    
    </div>
</div>