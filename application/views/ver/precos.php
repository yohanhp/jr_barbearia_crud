<div class="container">
    		<div class="row justify-content-center mb-5 pb-3 mt-5 pt-5">
          <div class="col-md-7 heading-section text-center ftco-animate">
            <h2 class="mb-4">Serviços &amp; Preços</h2>
            <p class="flip"><i class="fas fa-angle-left mr-2"></i><i class="fas fa-cut amber-text"></i><i class="fas fa-angle-right ml-2"></i></p>
            <p class="mt-5">Serviços exclusivamente para o público masculino.</p>
          </div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<div class="pricing-entry ftco-animate">
        			<div class="d-flex text align-items-center">
        				<h3><span>Corte De Cabelo Masculino</span></h3>
        				<span class="price text-warning">R$15.00</span>
        			</div>
        			<div class="d-block">
        				<p>Corte de cabelo de qualidade e seguindo as especificações do cliente</p>
        			</div>
        		</div>
        		<div class="pricing-entry ftco-animate">
        			<div class="d-flex text align-items-center">
        				<h3><span>Luzes</span></h3>
        				<span class="price text-warning">R$30.00</span>
        			</div>
        			<div class="d-block">
        				<p>Aplicação de luzes no cabelo</p>
        			</div>
        		</div>
        		<div class="pricing-entry ftco-animate">
        			<div class="d-flex text align-items-center">
        				<h3><span>Relaxamento</span></h3>
        				<span class="price text-warning">R$30.00</span>
        			</div>
        			<div class="d-block">
        				<p>Alisamento químico dos cabelos.</p>
        			</div>
        		</div>
        		<div class="pricing-entry ftco-animate">
        			<div class="d-flex text align-items-center">
        				<h3><span>Modelação e desenho da Barba</span></h3>
        				<span class="price text-warning">R$15.00</span>
        			</div>
        			<div class="d-block">
        				<p>Modelação e desenho da barba apartir do gosto do cliente. </p>
        			</div>
        		</div>
        	

        		<div class="pricing-entry ftco-animate">
        			<div class="d-flex text align-items-center">
        				<h3><span>Pigmentação da Barba</span></h3>
        				<span class="price text-warning">R$30.00</span>
        			</div>
				
        			<div class="d-block">
        				<p>Aplicação de pigmentação na barba.</p>
        			</div>
        		</div>
				
				<div class="pricing-entry ftco-animate">
        			<div class="d-flex text align-items-center">
        				<h3><span>Tintura da barba</span></h3>
        				<span class="price text-warning">R$30.00</span>
        			</div>
				
        			<div class="d-block">
        				<p>Aplicação de tintura na barba.</p>
        			</div>
        		</div>
				
				</div>
    	</div>
    </section>
