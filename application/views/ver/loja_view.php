<section class="ftco-section ftco-bg-dark">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate text-center">
            <h2 class="mb-4">Nossa Loja</h2>
            <p class="flip"><i class="fas fa-angle-left amber-text mr-2"></i><i class="fas fa-cut amber-text"></i><i class="fas fa-angle-right amber-text ml-2"></i></p>
            <p class="mt-5">Confira aqui alguns dos produtos que são vendidos na loja para que você possa cuidar do seu visual sem gastar muito.</p>
          </div>
        </div>
    		<div class="row">
    			<div class="col-md-4 ftco-animate">
    				<div class=" text-center">
    					<a href="loja.php?id=1"><img src="assets/images/produto1.jpg" class="img-fluid" alt="Produto 1"></a>
    					<div class="text">
    						<a href="loja.php?id=1"><p>Veja Mais</p></a>
    						<h3><a href="loja.php?id=1"><strong class="text-warning">Pomada Capilar</strong></a></h3>
    						<span class="price mb-4">R$15,00</span>
    					
    					</div>
    				</div>
    			</div>
    			<div class="col-md-4 ftco-animate">
    				<div class="text-center">
    					<a href="loja.php?id=2"><img src="assets/images/produto2.png" class="img-fluid" alt="Produto 2"></a>
    					<div class="text">
						<a href="loja.php?id=2"><p>Veja Mais</p></a>
						<h3><a href="loja.php?id=2"><strong class="text-warning">Gel Cola</strong></a></h3>
    						<span class="price mb-4">R$10,00</span>
    						
    					</div>
    				</div>
    			</div>
    			<div class="col-md-4 ftco-animate">
    				<div class="text-center">
    					<a href="loja.php?id=3"><img src="assets/images/produto3.png" class="img-fluid" alt="Produto 3"></a>
    					<div class="text">
						<a href="loja.php?id=3"><p>Veja Mais</p></a>
    						<h3><a href="loja.php?id=3"><strong class="text-warning">Loção pós-barba</strong></a></h3>
    						<span class="price mb-4">R$15,00</span>
    					
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
