    <section class="ftco-section ftco-discount img" style="background-image: url('<?= base_url('assets/images/banner.jpg'); ?>');" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row justify-content-center" data-scrollax-parent="true">
					<div class="col-md-7 text-center discount ftco-animate" data-scrollax=" properties: { translateY: '-30%'}">
						<h3>Corte Conosco e Saia Satisfeito</h3>
						<h2 class="mb-4 text-warning">Venha Conhecer Nossos Serviços</h2>
						<p class="mb-4">Obrigada a todos os clientes que acreditam
										no nosso trabalho. É com muito amor que cuidamos dessa
										nossa relação e estamos à disposição para ouvi-los sempre.</p>
						<button type="button" class="btn btn-outline-warning btn-rounded waves-effect"><a href="https://wa.me/5511980239587">Marque Conosco</button></a>
					</div>
				</div>
			</div>
		</section>