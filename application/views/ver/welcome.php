<section class="ftco-intro">
    	<div class="container-wrap mt-3">
    		<div class="d-md-flex align-items-end">
	    		<div class="container mx-auto">
					<div class="mx-auto">
	    			<div class="row no-gutters">
	    				<div class="col-md-4 d-flex ftco-animate">
	    					<div class="icon ml-5"><i class="fas fa-phone amber-text"></i></div>
	    					<div class="text ml-3">
								<h5>+55 (11) 98023-9587</h5>
	    						<p>Entre em contato</p>
	    					</div>
	    				</div>
	    				<div class="col-md-4 d-flex ftco-animate">
	    					<div class="icon ml-5"><i class="fas fa-map-marked amber-text"></i></div>
	    					<div class="text ml-3">
	    						<h5>Jardim Palmira, Guarulhos</h5>
	    						<p>R. Glauco Antônio Galli, 620</p>
	    					</div>
	    				</div>
	    				<div class="col-md-4 d-flex ftco-animate">
	    					<div class="icon ml-5"><i class="far fa-calendar-alt amber-text"></i></div>
	    					<div class="text ml-3">
	    						<h5>Abrimos de Segunda a Sexta</h5>
	    						<p>8:00am - 9:00pm</p>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
				</div>
    		</div>
    	</div>
    </section>
    <section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center mb-4">
          <div class="col-md-7 heading-section ftco-animate text-center">
            <h2 class="mb-4">Bem Vindo a <span class="amber-text">JR</span> Barbearia</h2>
            <p class="flip"><i class="fas fa-angle-left mr-2"></i><i class="fas fa-cut amber-text"></i><i class="fas fa-angle-right ml-2"></i></p>
          </div>
        </div>
    		<div class="row justify-content-center">
    			<div class="col-md-8 text-center ftco-animate">
    				<p>A família do dono (Junior Macedo) já vem do ramo de cortes de cabelo a muito tempo, um parente incentivou 
						Junior a fazer um curso de cortes em 2011, durante 6 meses ele praticou cortes caseiros. 
						Em 2012 passou a trabalhar com o tio, 1 ano depois passou a trabalhar num salão em jaçanã
						e 6 meses depois saiu com intuito de ter o próprio negocio. Em 2015 montou um salão que
						trabalhou com a família por 2 anos até que então em março de 2017 abriu o salão atual:<br>
						JR Barbearia.</p>
    			</div>
    		</div>
    	</div>
    </section>