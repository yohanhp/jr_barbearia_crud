<?php

include 'config/constants.php';

include 'views/header.php';
include 'views/navbar.php';

include 'sobre/views/sobre_view.php';
include 'home/views/proposta.php';
include 'home/views/barbeiro.php';
include 'home/views/numeros.php';

include 'views/rodape.php';
include 'views/footer.php';

?>
