<section class="ftco-section ftco-bg-dark">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate text-center">
            <h2 class="mb-4">Nossa Loja</h2>
            <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
            <p class="mt-5">Já dizia raffa moreira, doi um tapinha não doi só um tapinha sete sete sete, os vales verdes do ipiranga.</p>
          </div>
        </div>
    		<div class="row">
    			<div class="col-md-3 ftco-animate">
    				<div class="product-entry text-center">
    					<a href="#"><img src="images/prod-1.png" class="img-fluid" alt="Colorlib Template"></a>
    					<div class="text">
    						<p class="rate"><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star_half"></span></p>
    						<h3><a href="loja.php?id=1">Produto 01</a></h3>
    						<span class="price mb-4">$150</span>
    					
    					</div>
    				</div>
    			</div>
    			<div class="col-md-3 ftco-animate">
    				<div class="product-entry text-center">
    					<a href="#"><img src="images/prod-2.png" class="img-fluid" alt="Colorlib Template"></a>
    					<div class="text">
    						<p class="rate"><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star_half"></span></p>
    						<h3><a href="loja.php?id=2">Produto 02</a></h3>
    						<span class="price mb-4">R$150</span>
    						
    					</div>
    				</div>
    			</div>
    			<div class="col-md-3 ftco-animate">
    				<div class="product-entry text-center">
    					<a href="#"><img src="images/prod-3.png" class="img-fluid" alt="Colorlib Template"></a>
    					<div class="text">
    						<p class="rate"><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star_half"></span></p>
    						<h3><a href="loja.php?id=3">Produto 03</a></h3>
    						<span class="price mb-4">R$150</span>
    					
    					</div>
    				</div>
    			</div>
    			<div class="col-md-3 ftco-animate">
    				<div class="product-entry text-center">
    					<a href="#"><img src="images/prod-4.png" class="img-fluid" alt="Colorlib Template"></a>
    					<div class="text">
    						<p class="rate"><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star2"></span><span class="icon-star_half"></span></p>
    						<h3><a href="loja.php?id=4">Produto 04</a></h3>
    						<span class="price mb-4">R$150</span>
    						
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
