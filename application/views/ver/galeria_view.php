<div class="hero-wrap" style="background-image: url('<?= base_url('assets/images/bg_2.jpg'); ?>');'" data-stellar-background-ratio="0.5">
	      <div class="overlay"></div>
	      <div class="container">
	        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
	          <div class="col-md-6 text-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
	          	<h1 class="mb-3 mt-5 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Galeria</h1>
	            <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="<?= base_url(); ?>">Home</a></span> <span>Galeria</span></p>
	          </div>
	        </div>
	      </div>
	    </div>
	    <section class="ftco-gallery">
	    	<div class="container">
	    		<div class="row justify-content-center mb-5 pb-3">
                  <div class="col-md-7 heading-section ftco-animate text-center">
                    <h2 class="mb-4">Nossa Galeria</h2>
                    <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
                    <p class="mt-5">Algumas de nossas fotos.</p>
                  </div>
                </div>
	    	</div>
	    	<div class="container-wrap">
	    		<div class="row no-gutters">
						<div class="col-md-3">
				            <a class="gallery img d-flex align-items-center" style="background-image: url(<?= base_url('assets/images/gallery-1.jpg'); ?>);"></a>
						</div>
						<div class="col-md-3 ftco-animate">
							<a class="gallery img d-flex align-items-center" style="background-image: url(<?= base_url('assets/images/gallery-2.jpg'); ?>);"></a>
						</div>
						<div class="col-md-3 ftco-animate">
							<a class="gallery img d-flex align-items-center" style="background-image: url(<?= base_url('assets/images/gallery-3.jpg'); ?>);"></a>
						</div>
						<div class="col-md-3 ftco-animate">
							<a class="gallery img d-flex align-items-center" style="background-image: url(<?= base_url('assets/images/gallery-4.jpg'); ?>);"></a>
						</div>
						<div class="col-md-3 ftco-animate">
							<a class="gallery img d-flex align-items-center" style="background-image: url(<?= base_url('assets/images/gallery-5.jpg'); ?>);"></a>
						</div>
						<div class="col-md-3 ftco-animate">
							<a class="gallery img d-flex align-items-center" style="background-image: url(<?= base_url('assets/images/gallery-6.jpg'); ?>);"></a>
						</div>
						<div class="col-md-3 ftco-animate">
							<a class="gallery img d-flex align-items-center" style="background-image: url(<?= base_url('assets/images/gallery-7.jpg'); ?>);"></a>
						</div>
						<div class="col-md-3 ftco-animate">
							<a class="gallery img d-flex align-items-center" style="background-image: url(<?= base_url('assets/images/gallery-8.jpg'); ?>);"></a>
						</div>
	        </div>
	   </div>
	   </section>