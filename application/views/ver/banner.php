      
      <div class="hero-wrap js-fullheight" style="background-image: url('<?= base_url('assets/images/banner.jpg'); ?>');" data-stellar-background-ratio="0.5">
       <div class="text-white text-center d-flex align-items-center rgba-black-strong mx-auto py-5 px-4">
<div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="row justify-content-center mt-5 mb-5 pb-3" data-scrollax=" properties: { translateY: '70%' }"><br><br>
         
            <h1 class="mt-5 mb-5 text-center" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Precisa de cuidados? Venha cuidar do seu visual conosco </h1>
            <p class="mt-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><a href="https://wa.me/5511980239587"><button type="button" class="btn-outline-warning btn-rounded waves-effect px-3 py-2">Marque um Horário <i class="fab fa-whatsapp ml-1"></i></button></a></p>
          </div>
        </div>
      </div>
    </div></div>