

<section class="ftco-appointment">
			<div class="overlay"></div>
    	<div class="container-wrap">
    		<div class="row no-gutters d-md-flex align-items-center">
    			<div class="col-md-6 d-flex align-self-stretch">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d392.3058695548949!2d-46.56039142371128!3d-23.431014980218325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef45c1719f61f%3A0xee541fd8c01ee3b8!2sJr%20Barbearia!5e1!3m2!1spt-BR!2sbr!4v1568412965685!5m2!1spt-BR!2sbr" width="600" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    			</div>
	    		<div class="col-md-6 appointment ftco-animate">
	    			<h3 class="mb-3">Mande sua Mensagem</h3>
	    			<form method="POST" action="mensagem/salva_mensagem.php">
	    				<div class="d-md-flex">
		    				<div class="form-group">
							<input type="text" class="form-control" name="nome" placeholder="Nome Completo" maxlength="40" required>
		    				</div>
		    				<div class="form-group ml-md-4">
							<input type="email" class="form-control" name="email" placeholder="Seu melhor e-mail" maxlength="30" required>
		    				</div>
	    				</div>
	    				<div class="d-md-flex">
		    				<div class="form-group">
		    					<div class="input-wrap">
		            		<input type="text" class="form-control" name="assunto" placeholder="Assunto do contato" maxlength="20" required>
	            		</div>
		    				</div>

	    				</div>
	    				<div class="form-group">
						<textarea name="mensagem" cols="30" rows="3" class="form-control" placeholder="Mensagem" maxlength="300" required></textarea>
	            </div>
	            <div class="form-group">
	              <input type="submit" value="Enviar" class="btn btn-primary py-3 px-4">
	            </div>
	    			</form>
	    		</div>    			
    		</div>
    	</div>
    </section>
