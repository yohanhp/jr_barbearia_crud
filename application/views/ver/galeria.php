    <section class="ftco-gallery">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate text-center">
            <h2 class="mb-4">Nossa Galeria</h2>
            <p class="flip"><i class="fas fa-angle-left mr-2"></i><i class="fas fa-cut amber-text"></i><i class="fas fa-angle-right ml-2"></i></p>
            <p class="mt-5">Veja algumas de nossas fotos.</p>
          </div>
        </div>
    	</div>
    	<div class="container-wrap">
    		<div class="row no-gutters">
					<div class="col-md-3 ftco-animate view overlay img">
						<a class="gallery img d-flex align-items-center" style="background-image: url('<?= base_url('assets/images/corte3.jpg'); ?>'">
						<div class="mask flex-center rgba-black-strong">
    						<i class=" a fas fa-search text-warning"></i>
    					</div>
						</a>
					</div>
					<div class="col-md-3 ftco-animate view overlay img">
						<a class="gallery img d-flex align-items-center" style="background-image: url('<?= base_url('assets/images/banner.jpg'); ?>'">
						<div class="mask flex-center rgba-black-strong">
    						<i class=" a fas fa-search text-warning"></i>
    					</div>
						</a>
					</div>
					<div class="col-md-3 ftco-animate view overlay img">
						<a class="gallery img d-flex align-items-center" style="background-image: url('<?= base_url('assets/images/corte2.jpg'); ?>'">
						<div class="mask flex-center rgba-black-strong">
    						<i class=" a fas fa-search text-warning"></i>
    					</div>
						</a>
					</div>
					<div class="col-md-3 ftco-animate view overlay img">
						<a class="gallery img d-flex align-items-center" style="background-image: url('<?= base_url('assets/images/corte1.jpg'); ?>'">
						<div class="mask flex-center rgba-black-strong">
    						<i class=" a fas fa-search text-warning"></i>
    					</div>
						</a>
					</div>
        </div>
		</div>
		<div class="mt-5 md-5 mx-auto ftco-animate">
		           <p class="flip text-center"><i class="fas fa-angle-left mr-2"></i><i class="fas fa-cut amber-text"></i><i class="fas fa-angle-right ml-2"></i></p>
</div>
    </section>
