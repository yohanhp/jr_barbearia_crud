<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logar extends CI_Controller {

	public function index(){
		$this->db->select('*');
		$dados['usuarios'] = $this->db->get('usuario')->result();
		$this->load->view('template/header');
		$this->load->view('template/navbar');
		$this->load->view('ver/login');
		$this->load->model('UserModel', 'model');

		$e = $this->input->post("email");
		$s = $this->input->post("senha");
		$user = $this->model->logar($e, $s);

		if($user){
			redirect('logar/admin');
		}
		$this->load->view('template/rodape');
	}

	public function admin(){
		$this->db->select('*');
		$dados['usuarios'] = $this->db->get('usuario')->result();
		$this->load->view('template/navbar_cadastro');
		$this->load->view('template/header');
		$this->load->view('ver/construcao');
		$this->load->view('ver/cadastro');
		$this->load->view('template/rodape');
		$this->load->view('ver/conexao');
	}

	public function cadastrar(){
        $data['nome'] = $this->input->post('nome');
        $data['email'] = $this->input->post('email');
        $data['senha'] = md5($this->input->post('senha'));
		$data['telefone'] = $this->input->post('telefone');
		$data['nivel'] = $this->input->post('nivel');
		$this->load->view('template/navbar_cadastro');
		$this->load->view('template/header');
	
		if ($this->db->insert('usuario', $data)){}	
	}
    
    public function lista(){
    $this->load->view('template/navbar_cadastro');
    $this->load->view('template/header');
    $this->load->model('UserModel', 'model');
    $data['tabela'] = $this->model->gera_tabela();
    $this->load->view('common/table.php', $data); 
    }
    
    public function deletar($id){
        $this->load->model('UserModel', 'model');
        $this->model->deletar($id);
        redirect('logar/lista');
    }
    
    public function editar($id){
        $this->load->view('template/navbar_cadastro');
        $this->load->view('template/header');
        $this->load->model('UserModel', 'model');
        $data['user'] = $this->model->read($id);
        $this->load->view('ver/cadastro', $data);
        $this->model->edita_usuario($id);
        $this->load->view('template/rodape');
    }

	public function logout(){
		$this->session->sess_destroy();
        redirect('logar');
	}

}