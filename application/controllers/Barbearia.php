<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Barbearia extends CI_Controller{

	public function index()
	{
		$this->load->view('template/header');
        $this->load->view('template/navbar');

        $this->load->view('ver/banner');
        $this->load->view('ver/welcome');
        $this->load->view('ver/servicos');
        $this->load->view('ver/fazemos');
        $this->load->view('ver/precos');
        $this->load->view('ver/proposta');
        $this->load->view('ver/barbeiro');
        $this->load->view('ver/loja_view');
        $this->load->view('ver/galeria');
        //$this->load->view('ver/numeros');
        //$this->load->view('ver/blog');
        //$this->load->view('ver/compromisso');

		$this->load->view('template/footer');
		$this->load->view('template/rodape');
    }

	public function sobre()
	{
		$this->load->view('template/header');
        $this->load->view('template/navbar');
        
        $this->load->view('ver/sobre_view');
        $this->load->view('ver/proposta');
        $this->load->view('ver/barbeiro');
        $this->load->view('ver/numeros');

		$this->load->view('template/footer');
		$this->load->view('template/rodape');
    }

    public function servicos()
	{
		$this->load->view('template/header');
        $this->load->view('template/navbar');
        
        $this->load->view('ver/servicos_view');
        $this->load->view('ver/servicos');
        $this->load->view('ver/fazemos');
        $this->load->view('ver/precos');
        $this->load->view('ver/servico_marque');

		$this->load->view('template/footer');
		$this->load->view('template/rodape');
    }

    public function galeria()
	{
		$this->load->view('template/header');
        $this->load->view('template/navbar');
        
        $this->load->view('ver/galeria_view');

		$this->load->view('template/footer');
		$this->load->view('template/rodape');
    }

    public function blog()
	{
		$this->load->view('template/header');
        $this->load->view('template/navbar');
        
        $this->load->view('ver/blog_view');

		$this->load->view('template/footer');
		$this->load->view('template/rodape');
    }
   
    public function contato()
	{
		$this->load->view('template/header');
        $this->load->view('template/navbar');
        
        $this->load->view('ver/contato_view');

		$this->load->view('template/footer');
		$this->load->view('template/rodape');
    }
}